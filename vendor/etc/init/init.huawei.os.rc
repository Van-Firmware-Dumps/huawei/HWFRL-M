service hwpged /system/bin/hwpged
    class main
    socket iawared seqpacket 0660 system system
    user root
    group system readproc
    writepid /dev/cpuset/foreground/tasks

service powerlogd /system/bin/powerlogd
    class core
    socket powerlogd stream 0600 system system
    socket powerlogdr seqpacket 0600 system system
    socket powerlogdw dgram 0220 system system
    writepid /dev/cpuset/system-background/tasks /dev/cpuctl/bg_non_interactive/tasks /dev/stune/system-background/tasks /dev/blkio/system-background/tasks

service mapper /vendor/bin/sh /vendor/bin/mappersh
    class late_start
    user root
    group system
    disabled
    seclabel u:r:shell:s0

service ddrtest /vendor/bin/do_ddrtest
    user root
    disabled
    oneshot

service bastetd /system/bin/bastetd
    class main
    user root
    group system root
    disabled

service stop_ddrtest /vendor/bin/do_ddrtest
    user root
    disabled
    oneshot

service hwemerffu /vendor/bin/hwemerffu
    class main
    user root
    group root system
    oneshot

service storage_info /vendor/bin/storage_info
    restart_period 600
    user root
    group system wakelock

service mtk-post-boot /vendor/bin/init_mtk_post_boot.sh
    class late_start
    user root
    disabled
    oneshot

on property:vendor.debug.rt.ddr.test=1
    stop ddrtest
    start ddrtest
on property:vendor.debug.rt.ddr.test=2
    start ddrtest
on property:vendor.debug.rt.ddr.test=3
    start ddrtest
on property:vendor.debug.rt.ddr.test=4
    start stop_ddrtest
on property:vendor.debug.rt.ddr.test=5
    stop ddrtest
    start ddrtest
on property:vendor.debug.rt.ddr.test=6
    start ddrtest

on property:sys.boot_completed=1
    start mtk-post-boot

on property:odm.bastet.service.enable=true
    start bastetd

on init

    #Create cgroup mount point for freezer
    mkdir /dev/frz
    mount cgroup none /dev/frz freezer
    chmod 0775 /dev/frz

    #add for zrhung huawei_hung_task
    write /proc/sys/kernel/hung_task_panic 1
    write /proc/sys/kernel/hung_task_timeout_secs 120
    write /sys/kernel/hungtask/monitorlist "whitelist,system_server,surfaceflinger,init"
    write /sys/kernel/hungtask/enable "on"
    chown system system /sys/kernel/hungtask/vm_heart
    symlink /dev/block/platform/bootdevice/by-name/recovery_vendor  /dev/block/platform/bootdevice/by-name/recovery
    #for cpuset cgroup
    mkdir /dev/cpuset/key-background
    copy /dev/cpuset/cpus /dev/cpuset/key-background/cpus
    copy /dev/cpuset/mems /dev/cpuset/key-background/mems

    chown system system /dev/cpuset/key-background
    chown system system /dev/cpuset/key-background/tasks

    chmod 0664 /dev/cpuset/key-background/tasks

    #for cpu control cgroup
    mkdir /dev/cpuctl/limit
    chown system system /dev/cpuctl/limit/tasks
    chmod 0644 /dev/cpuctl/limit/tasks
    write /dev/cpuctl/limit/cpu.shares 1024
    write /dev/cpuctl/limit/cpu.rt_runtime_us 800000
    write /dev/cpuctl/limit/cpu.rt_period_us 1000000
    # sets up initial workingset cgroup
    mkdir /dev/workingset
    mount cgroup none /dev/workingset nodev noexec nosuid workingset
    mkdir /dev/workingset/monitor0
    mkdir /dev/workingset/monitor1

    chmod 0600 /dev/workingset/tasks
    chmod 0600 /dev/workingset/monitor0/tasks
    chmod 0600 /dev/workingset/monitor1/tasks
    chmod 0600 /dev/workingset/cgroup.procs
    chmod 0600 /dev/workingset/monitor0/cgroup.procs
    chmod 0600 /dev/workingset/monitor1/cgroup.procs
    chmod 0600 /dev/workingset/monitor0/workingset.state
    chmod 0600 /dev/workingset/monitor1/workingset.state
    chmod 0600 /dev/workingset/monitor0/workingset.data
    chmod 0600 /dev/workingset/monitor1/workingset.data

# sets up initial protectLru memcg
    mkdir /dev/memcg/protect_lru_1
    mkdir /dev/memcg/protect_lru_2
    mkdir /dev/memcg/protect_lru_3
    chmod 0400 /dev/memcg/protect_lru_1
    chmod 0400 /dev/memcg/protect_lru_2
    chmod 0400 /dev/memcg/protect_lru_3

# Added for qosctrl begin
    chown system system /dev/iaware_qos_ctrl
    chmod 660 /dev/iaware_qos_ctrl

# rcc module
    chown system system /sys/kernel/rcc/enable
    chown system system /sys/kernel/rcc/event
    chown system system /sys/kernel/rcc/idle_threshold
    chown system system /sys/kernel/rcc/swap_percent_low
    chown system system /sys/kernel/rcc/free_size_min
    chown system system /sys/kernel/rcc/full_clean_size
    chown system system /sys/kernel/rcc/stat
    chown system system /sys/kernel/rcc/can_compress
    chown system system /sys/kernel/rcc/avail_target
    chown system system /sys/kernel/rcc/anon_target
    chown system system /sys/kernel/rcc/force_once
    chown system system /sys/kernel/rcc/max_anon_clean_size

    write /sys/kernel/rcc/event PASSIVE_MODE
    write /sys/kernel/rcc/max_anon_clean_size 512M
    write /sys/kernel/rcc/enable 1

on property:sys.boot_completed=1
    write /sys/kernel/rcc/event BOOT_COMPLETE

on early-boot
    start hwemerffu

on boot
    chmod 0755 /sys/kernel/debug/tracing
#Touchscreen sysfs
    chown system system /sys/touchscreen/ic_ver
    chown system system /sys/touchscreen/hw_reset
    chown system system /sys/touchscreen/hw_irq_stat
    chown system system /sys/touchscreen/drv_debug
    chown system system /sys/touchscreen/sleep_status
    chown system system /sys/touchscreen/wakeup_gesture_enable
    chown system system /sys/touchscreen/easy_wakeup_gesture
    chown system system /sys/touchscreen/easy_wakeup_control
    chown system system /sys/touchscreen/easy_wakeup_position
    chown system system /sys/touchscreen/easy_wakeup_supported_gestures
    chown system system /sys/touchscreen/touch_glove
    chown system system /sys/touchscreen/touch_window
    chown system system /dev/rmi0
    chown system system /sys/touchscreen/touch_sensitivity
    chown system system /sys/touchscreen/fw_update_sd
    chown system system /sys/touchscreen/reset
    chown system system /sys/touchscreen/touch_chip_info
    chown system system /sys/touchscreen/touch_oem_info
    chown system system /sys/touchscreen/roi_enable
    chown system system /sys/touchscreen/roi_data
    chown system system /sys/touchscreen/roi_data_debug
    chown system system /sys/touchscreen/hw_mmi_index
    chown system system /sys/touchscreen/cyttsp5_device_access.main_ttsp_core/rawdata_check
    chown system system /sys/touchscreen/cyttsp5_device_access.main_ttsp_core/command
    chown system system /sys/touchscreen/cyttsp5_device_access.main_ttsp_core/response
    chown system system /proc/touchscreen/data
    chown system system /proc/touchscreen/dumpdata
    chown system system /proc/touchscreen/rawdata
    chown system system /proc/touchscreen/tp_capacitance_data
    chown system system /sys/devices/platform/huawei_tp_color/tp_color_info
    chown system system /sys/touchscreen/tp_capacitance_test_type
    chown system system /sys/touchscreen/supported_func_indicater
    chown system system /sys/touchscreen/tp_capacitance_test_config
    chown system system /sys/touchscreen/calibrate
    chown system system /sys/touchscreen/calibrate_wakeup_gesture
    chown system system /sys/touchscreen/touch_rawdata_debug
    chown system system /sys/touchscreen/touch_special_hardware_test
    chown system system /sys/touchscreen/touch_register_operation
    chown system system /sys/touchscreen/touch_wideth
    chown system system /sys/touchscreen/touch_switch
    chown system system /sys/touchscreen/udfp_enable
    chown system system /sys/touchscreen/stylus_wakeup_ctrl
    chown system system /sys/touchscreen/aod_touch_switch_ctrl
    chown system system /sys/touchscreen/fingerprint_switch_ctrl
    chown system system /sys/touchscreen/power_switch_ctrl
    chown system system /sys/touchscreen/aod_event_report_ctrl
    chown system system /sys/touchscreen/tp_ud_lowpower_ctrl
    chown system system /sys/touchscreen/aod_notify_tp_power_ctrl

#lcd sysfs
    chown system system /sys/class/graphics/fb0/lcd_cabc_mode
    chown system system /sys/class/graphics/fb0/lcd_ce_mode
    chown system system /sys/class/graphics/fb0/lcd_se_mode
    chown system system /sys/class/graphics/fb0/lcd_checksum
    chown system system /sys/class/graphics/fb0/lcd_check_reg
    chown system system /sys/class/graphics/fb0/lcd_inversion_mode
    chown system system /sys/class/graphics/fb0/lcd_model
    chown system system /sys/class/graphics/fb0/lcd_display_type
    chown system system /sys/class/graphics/fb0/lcd_voltage_enable
    chown system system /sys/class/graphics/fb0/lcd_fps_scence
    chown system system /sys/class/graphics/fb0/lcd_fps_order
    chown system system /sys/class/graphics/fb0/lcd_status
    chown system system /sys/class/graphics/fb0/lcd_sleep_ctrl
    chown system system /sys/class/graphics/fb0/lcd_func_switch
    chown system system /sys/class/graphics/fb0/lcd_test_config
    chown system system /sys/class/graphics/fb0/lcd_support_mode
    chown system system /sys/class/graphics/fb0/lcd_ic_color_enhancement_mode
    chown system system /sys/class/graphics/fb0/lcd_support_checkmode
    chown system system /sys/class/graphics/fb0/panel_info
    chown system system /sys/class/graphics/fb0/lcd_scan_mode
    chown system system /sys/class/graphics/fb0/lcd_reg_read
    chown system system /sys/class/graphics/fb0/lcd_bl_mode
    chown system system /sys/class/graphics/fb0/lcd_bl_support_mode
    chown system system /sys/class/graphics/fb0/ddic_oem_info
    chown system system /sys/class/graphics/fb0/lcd_mipi_config
    chown system system /sys/class/graphics/fb0/alpm_setting
    chown system system /sys/class/graphics/fb0/display_idle_mode
    chown system system /sys/firmware/devicetree/base/huawei,lcd_config
    chmod 0640 /sys/firmware/devicetree/base/huawei,lcd_config
    chown system system /proc/device-tree/huawei,lcd_config
    chmod 0640 /proc/device-tree/huawei,lcd_config
    chown system system /sys/devices/platform/huawei,lcd_config
    chmod 0640 /sys/devices/platform/huawei,lcd_config
    chown system system /sys/bus/platform/devices
    chmod 0640 /sys/bus/platform/devices
    chwon system system /sys/bus/platform/drivers/lcd_kit_mipi_panel
    chmod 0640 /sys/bus/platform/drivers/lcd_kit_mipi_panel

#fingerprint
    chmod 0660 /dev/fingerprint
    chown system system /dev/fingerprint
    chmod 0660 /sys/devices/platform/fingerprint/irq
    chown system system /sys/devices/platform/fingerprint/irq
    chmod 0660 /sys/devices/platform/fingerprint/result
    chown system system /sys/devices/platform/fingerprint/result
    chown system system /sys/devices/platform/fingerprint/fingerprint_chip_info
    chmod 0660 /sys/devices/platform/fingerprint/read_image_flag
    chown system system /sys/devices/platform/fingerprint/read_image_flag
    chmod 0660 /sys/devices/platform/fingerprint/nav
    chown system system /sys/devices/platform/fingerprint/nav
    chmod 0660 /sys/devices/platform/fingerprint/snr
    chown system system /sys/devices/platform/fingerprint/snr
    chmod 0660 /sys/devices/platform/fingerprint/module_id
    chown system system /sys/devices/platform/fingerprint/module_id
    chmod 0660 /sys/devices/platform/fingerprint/module_id_ud
    chown system system /sys/devices/platform/fingerprint/module_id_ud
    chmod 0660 /sys/devices/platform/fingerprint/low_temperature
    chown system system /sys/devices/platform/fingerprint/low_temperature
    chmod 0660 /sys/devices/platform/fingerprint/ud_fingerprint_chip_info
    chown system system /sys/devices/platform/fingerprint/ud_fingerprint_chip_info
    mkdir /data/vendor/log/fingerprint
    chown system system /data/vendor/log/fingerprint
    restorecon_recursive /hw_product/bin/fingerprint
    mkdir /data/vendor/fingerprint_dump 0770 system system
    chown system system /data/vendor/fingerprint_dump
    restorecon_recursive /data/vendor/fingerprint_dump
#hwbfm
    chown system system /dev/hw_bfm
    chmod 0660 /dev/hw_bfm
#cpu idle
    chown system system /proc/mcdi/state
    chmod 0664 /proc/mcdi/state
    chown system system /proc/cpuidle/enable
    chmod 0664 /proc/cpuidle/enable

# top n memory process
    chown system system /proc/driver/process_mem
#zram
    chown system system /sys/block/zram0/idle
    chown system system /sys/block/zram0/writeback
    chown system system /sys/block/zram0/writeback_disable
    chown system system /sys/block/zram0/writeback_limit
    chown system system /sys/block/zram0/writeback_limit_enable
    chown system system /sys/block/zram0/writeback_limit_cycle
    chown system system /sys/block/zram0/writeback_limit_max
    chown system system /sys/block/zram0/dedup_enable
    chown system system /sys/block/zram0/noncompress_enable
#add for antenna cable detect
    chmod 0660 /sys/class/antenna_cable/detect/state
    chown system radio /sys/class/antenna_cable/detect/state

on fs
    restorecon /log
    chmod 775 /log
    chown root system /log

on post-fs-data
#charger
    chmod 0664 /sys/class/hw_power/charger/charge_data/iin_thermal
    chown system system /sys/class/hw_power/charger/charge_data/iin_thermal
    chmod 0664 /sys/class/hw_power/charger/charge_data/iin_runningtest
    chown system system /sys/class/hw_power/charger/charge_data/iin_runningtest
    chmod 0664 /sys/class/hw_power/charger/charge_data/iin_rt_current
    chown system system /sys/class/hw_power/charger/charge_data/iin_rt_current
    chmod 0664 /sys/class/hw_power/charger/charge_data/enable_hiz
    chown system system /sys/class/hw_power/charger/charge_data/enable_hiz
    chmod 0664 /sys/class/hw_power/charger/charge_data/shutdown_watchdog
    chown system system /sys/class/hw_power/charger/charge_data/shutdown_watchdog
    chmod 0664 /sys/class/hw_power/charger/charge_data/enable_charger
    chown system system /sys/class/hw_power/charger/charge_data/enable_charger
    chmod 0664 /sys/class/hw_power/charger/charge_data/factory_diag
    chown system system /sys/class/hw_power/charger/charge_data/factory_diag
    chmod 0660 /sys/class/hw_power/charger/charge_data/update_volt_now
    chown system system /sys/class/hw_power/charger/charge_data/update_volt_now
    chmod 0664 /sys/class/hw_power/adapter/test_result
    chown system system /sys/class/hw_power/adapter/test_result
    chmod 0660 /sys/class/hw_power/charger/charge_data/ichg_ratio
    chown system system /sys/class/hw_power/charger/charge_data/ichg_ratio
    chmod 0660 /sys/class/hw_power/charger/charge_data/vterm_dec
    chown system system /sys/class/hw_power/charger/charge_data/vterm_dec
    chmod 0660 /sys/class/hw_power/charger/charge_data/thermal_reason
    chown system system /sys/class/hw_power/charger/charge_data/thermal_reason
    chmod 0660 /sys/class/hw_power/interface/enable_charger
    chown system system /sys/class/hw_power/interface/enable_charger
    chmod 0660 /sys/class/hw_power/interface/ichg_limit
    chown system system /sys/class/hw_power/interface/ichg_limit
    chmod 0660 /sys/class/hw_power/interface/ichg_ratio
    chown system system /sys/class/hw_power/interface/ichg_ratio
    chmod 0660 /sys/class/hw_power/interface/vterm_dec
    chown system system /sys/class/hw_power/interface/vterm_dec
    chmod 0660 /sys/class/hw_power/interface/enable_debug
    chown system system /sys/class/hw_power/interface/enable_debug
    chmod 0660 /sys/class/hw_power/interface/adap_volt
    chown system system /sys/class/hw_power/interface/adap_volt
    chmod 0660 /sys/class/hw_power/interface/wl_thermal_ctrl
    chown system system /sys/class/hw_power/interface/wl_thermal_ctrl
    chmod 0220 /sys/class/hw_power/interface/iin_thermal
    chown system system /sys/class/hw_power/interface/iin_thermal
    chmod 0220 /sys/class/hw_power/interface/iin_thermal_all
    chown system system /sys/class/hw_power/interface/iin_thermal_all
    chmod 0660 /sys/class/hw_power/interface/ichg_thermal
    chown system system /sys/class/hw_power/interface/ichg_thermal
    chmod 0440 /sys/class/hw_power/interface/ibus
    chown system system /sys/class/hw_power/interface/ibus
    chmod 0440 /sys/class/hw_power/interface/vbus
    chown system system /sys/class/hw_power/interface/vbus
    chmod 0660 /sys/class/hw_power/interface/rt_test_time
    chown system system /sys/class/hw_power/interface/rt_test_time
    chmod 0660 /sys/class/hw_power/interface/rt_test_result
    chown system system /sys/class/hw_power/interface/rt_test_result
    chmod 0220 /sys/class/hw_power/interface/rtb_success
    chown system system /sys/class/hw_power/interface/rtb_success
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/set_resistance_threshold
    chown system system /sys/class/hw_power/charger/direct_charger_sc/set_resistance_threshold
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/set_chargetype_priority
    chown system system /sys/class/hw_power/charger/direct_charger_sc/set_chargetype_priority
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/thermal_reason
    chown system system /sys/class/hw_power/charger/direct_charger_sc/thermal_reason
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/iin_thermal_ichg_control
    chown system system /sys/class/hw_power/charger/direct_charger_sc/iin_thermal_ichg_control
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/ichg_control_enable
    chown system system /sys/class/hw_power/charger/direct_charger_sc/ichg_control_enable
    chmod 0440 /sys/class/hw_power/charger/direct_charger_sc/sc_state
    chown system system /sys/class/hw_power/charger/direct_charger_sc/sc_state
    chmod 0660 /sys/class/hw_power/charger/direct_charger/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger/thermal_reason
    chown system system /sys/class/hw_power/charger/direct_charger/thermal_reason
    chmod 0660 /sys/class/hw_power/charger/direct_charger/iin_thermal_ichg_control
    chown system system /sys/class/hw_power/charger/direct_charger/iin_thermal_ichg_control
    chmod 0660 /sys/class/hw_power/charger/direct_charger/ichg_control_enable
    chown system system /sys/class/hw_power/charger/direct_charger/ichg_control_enable
    chmod 0222 /sys/class/hw_power/soc_decimal/start
    chown system system /sys/class/hw_power/soc_decimal/start
    chmod 0444 /sys/class/hw_power/soc_decimal/soc
    chown system system /sys/class/hw_power/soc_decimal/soc
    chmod 0444 /sys/class/hw_power/power_ui/cable_type
    chown system system /sys/class/hw_power/power_ui/cable_type
    chmod 0444 /sys/class/hw_power/power_ui/icon_type
    chown system system /sys/class/hw_power/power_ui/icon_type
    chmod 0444 /sys/class/hw_power/power_ui/max_power
    chown system system /sys/class/hw_power/power_ui/max_power
    chmod 0444 /sys/class/hw_power/power_ui/wl_off_pos
    chown system system /sys/class/hw_power/power_ui/wl_off_pos
    chmod 0444 /sys/class/hw_power/power_ui/wl_fan_status
    chown system system /sys/class/hw_power/power_ui/wl_fan_status
    chmod 0444 /sys/class/hw_power/power_ui/wl_cover_status
    chown system system /sys/class/hw_power/power_ui/wl_cover_status
    chmod 0444 /sys/class/hw_power/power_ui/water_status
    chown system system /sys/class/hw_power/power_ui/water_status
    chmod 0444 /sys/class/hw_power/power_ui/heating_status
    chown system system /sys/class/hw_power/power_ui/heating_status
    chmod 0444 /sys/class/hw_power/lga_ck/status
    chown system system /sys/class/hw_power/lga_ck/status
    chmod 0440 /sys/devices/platform/huawei_ptst/wlc_mmi/result
    chown system system /sys/devices/platform/huawei_ptst/wlc_mmi/result
    chmod 0440 /sys/devices/platform/huawei_ptst/wlc_mmi/timeout
    chown system system /sys/devices/platform/huawei_ptst/wlc_mmi/timeout
    chmod 0660 /sys/devices/platform/huawei_ptst/wlc_mmi/start
    chown system system /sys/devices/platform/huawei_ptst/wlc_mmi/start
    chmod 0660 /sys/devices/platform/huawei_ptst/dc_mmi/test_status
    chown system system /sys/devices/platform/huawei_ptst/dc_mmi/test_status
    chmod 0444 sys/kernel/debug/power_debug/total_ibus
    chown system system sys/kernel/debug/power_debug/total_ibus
    chmod 0444 sys/kernel/debug/power_debug/main_ibus
    chown system system sys/kernel/debug/power_debug/main_ibus
    chmod 0444 sys/kernel/debug/power_debug/main_vbus
    chown system system sys/kernel/debug/power_debug/main_vbus
    chmod 0444 sys/kernel/debug/power_debug/main_vout
    chown system system sys/kernel/debug/power_debug/main_vout
    chmod 0444 sys/kernel/debug/power_debug/main_ibat
    chown system system sys/kernel/debug/power_debug/main_ibat
    chmod 0444 sys/kernel/debug/power_debug/main_vbat
    chown system system sys/kernel/debug/power_debug/main_vbat
    chmod 0444 sys/kernel/debug/power_debug/main_ic_temp
    chown system system sys/kernel/debug/power_debug/main_ic_temp
    chmod 0444 sys/kernel/debug/power_debug/main_ic_id
    chown system system sys/kernel/debug/power_debug/main_ic_id
    chmod 0444 sys/kernel/debug/power_debug/main_ic_name
    chown system system sys/kernel/debug/power_debug/main_ic_name
    chmod 0444 sys/kernel/debug/power_debug/aux_ibus
    chown system system sys/kernel/debug/power_debug/aux_ibus
    chmod 0444 sys/kernel/debug/power_debug/aux_vbus
    chown system system sys/kernel/debug/power_debug/aux_vbus
    chmod 0444 sys/kernel/debug/power_debug/aux_vout
    chown system system sys/kernel/debug/power_debug/aux_vout
    chmod 0444 sys/kernel/debug/power_debug/aux_ibat
    chown system system sys/kernel/debug/power_debug/aux_ibat
    chmod 0444 sys/kernel/debug/power_debug/aux_vbat
    chown system system sys/kernel/debug/power_debug/aux_vbat
    chmod 0444 sys/kernel/debug/power_debug/aux_ic_temp
    chown system system sys/kernel/debug/power_debug/aux_ic_temp
    chmod 0444 sys/kernel/debug/power_debug/aux_ic_id
    chown system system sys/kernel/debug/power_debug/aux_ic_id
    chmod 0444 sys/kernel/debug/power_debug/aux_ic_name
    chown system system sys/kernel/debug/power_debug/aux_ic_name
    chmod 0444 sys/class/hw_power/adapter/adapter_type
    chown system system sys/class/hw_power/adapter/adapter_type
    chmod 0444 sys/kernel/debug/power_debug/tadapt
    chown system system sys/kernel/debug/power_debug/tadapt
    chmod 0444 sys/kernel/debug/power_debug/vadapt
    chown system system sys/kernel/debug/power_debug/vadapt
    chmod 0444 sys/kernel/debug/power_debug/tusb
    chown system system sys/kernel/debug/power_debug/tusb
    chmod 0444 sys/kernel/debug/power_debug/lvc_ibus
    chown system system sys/kernel/debug/power_debug/lvc_ibus
    chmod 0444 sys/kernel/debug/power_debug/lvc_vbus
    chown system system sys/kernel/debug/power_debug/lvc_vbus
    chmod 0444 sys/kernel/debug/power_debug/lvc_ibat
    chown system system sys/kernel/debug/power_debug/lvc_ibat
    chmod 0444 sys/kernel/debug/power_debug/lvc_vbat
    chown system system sys/kernel/debug/power_debug/lvc_vbat
    chmod 0444 sys/kernel/debug/power_debug/lvc_ic_id
    chown system system sys/kernel/debug/power_debug/lvc_ic_id
    chmod 0444 sys/kernel/debug/power_debug/lvc_ic_name
    chown system system sys/kernel/debug/power_debug/lvc_ic_name
    chmod 0444 sys/kernel/debug/power_debug/lvc_vadapt
    chown system system sys/kernel/debug/power_debug/lvc_vadapt
    chmod 0660 /sys/class/hw_power/charger/wireless_sc/iin_thermal
    chown system system /sys/class/hw_power/charger/wireless_sc/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/vrect
    chown system system /sys/class/hw_power/charger/wireless_charger/vrect
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/vout
    chown system system /sys/class/hw_power/charger/wireless_charger/vout
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/iout
    chown system system /sys/class/hw_power/charger/wireless_charger/iout
    chmod 0440 /sys/class/hw_power/charger/wireless_charger/tx_adaptor_type
    chown system system /sys/class/hw_power/charger/wireless_charger/tx_adaptor_type
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/program_otp
    chown system system /sys/class/hw_power/charger/wireless_charger/program_otp
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/en_enable
    chown system system /sys/class/hw_power/charger/wireless_charger/en_enable
    chmod 0440 /sys/class/hw_power/charger/wireless_charger/wireless_succ
    chown system system /sys/class/hw_power/charger/wireless_charger/wireless_succ
    chmod 0440 /sys/class/hw_power/charger/wireless_charger/normal_chrg_succ
    chown system system /sys/class/hw_power/charger/wireless_charger/normal_chrg_succ
    chmod 0440 /sys/class/hw_power/charger/wireless_charger/fast_chrg_succ
    chown system system /sys/class/hw_power/charger/wireless_charger/fast_chrg_succ
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/fod_coef
    chown system system /sys/class/hw_power/charger/wireless_charger/fod_coef
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/interference_setting
    chown system system /sys/class/hw_power/charger/wireless_charger/interference_setting
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/rx_support_mode
    chown system system /sys/class/hw_power/charger/wireless_charger/rx_support_mode
    chmod 0660 /sys/class/hw_power/charger/wireless_charger/thermal_ctrl
    chown system system /sys/class/hw_power/charger/wireless_charger/thermal_ctrl
    chmod 0660 /sys/class/hw_power/charger/wireless_tx/tx_open
    chown system system /sys/class/hw_power/charger/wireless_tx/tx_open
    chmod 0660 /sys/class/hw_power/charger/wireless_aux_tx/tx_open
    chown system system /sys/class/hw_power/charger/wireless_aux_tx/tx_open
    chmod 0440 /sys/class/hw_power/charger/wireless_tx/tx_status
    chown system system /sys/class/hw_power/charger/wireless_tx/tx_status
    chmod 0440 /sys/class/hw_power/charger/wireless_tx/tx_iin_avg
    chown system system /sys/class/hw_power/charger/wireless_tx/tx_iin_avg
    chmod 0444 /sys/class/hw_power/wireless/tx_bd_info
    chown system system /sys/class/hw_power/wireless/tx_bd_info

#Camera
    chown system system /sys/class/flashlight_core/flashlight/flash_lightness
    chmod 0660 /sys/class/flashlight_core/flashlight/flash_lightness
    mkdir /data/vendor/camera
    chown system camera /data/vendor/camera
    chmod 770 /data/vendor/camera
    chmod 664 /data/vendor/camera/mtk_sensor0
    chmod 664 /data/vendor/camera/mtk_sensor1
    chmod 664 /data/vendor/camera/mtk_sensor2
    chmod 664 /data/vendor/camera/backcamerasn
    chmod 664 /data/vendor/camera/frontcamerasn
    chmod 660 /sys/class/leds/torch/flash_thermal_protect
    chown system system /sys/class/leds/torch/flash_thermal_protect
#usb
    chown system system /sys/class/android_usb/android0/switch_request
    chmod 0664 /sys/class/android_usb/android0/switch_request
#log
    mkdir /log/reliability
    chmod 0775 /log/reliability
    chown root system /log/reliability

    mkdir /log/reliability/boot_fail
    chmod 0775 /log/reliability/boot_fail
    chown root system /log/reliability/boot_fail
    restorecon_recursive /log/reliability
#tp_data
    mkdir /data/vendor/log/tp_data
    chmod 0775 /data/vendor/log/tp_data
    chown root system /data/vendor/log/tp_data
#xcollie
    mkdir /data/vendor/log/reliability
    chmod 0775 /data/vendor/log/reliability
    chown root system /data/vendor/log/reliability

    mkdir /data/vendor/log/reliability/xcollie
    chown system log /data/vendor/log/reliability/xcollie
    chmod 2775 /data/vendor/log/reliability/xcollie
    restorecon_recursive /data/vendor/log/reliability/xcollie
#storage_info
    mkdir /log/storage
    chmod 0770 /log/storage
    chown root system /log/storage
    restorecon_recursive /log/storage
    start storage_info

on property:sys.boot_completed=1
    chown system  system /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us
    chown system  system /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
    chmod 660 /sys/devices/system/cpu/cpufreq/schedutil/up_rate_limit_us
    chmod 660 /sys/devices/system/cpu/cpufreq/schedutil/down_rate_limit_us
    write /proc/self/reclaim file

on nonencrypted
    trigger data_ready

on data_ready
    exec u:r:blkcginit:s0 root root -- /vendor/bin/sh /vendor/etc/blkcg_init.sh 0 0

on property:persist.sys.huawei.debug.on=*
    write /proc/sys/kernel/sysrq ${persist.sys.huawei.debug.on}
